import React, { useState } from 'react';
import Weather from './weather.jsx'
import axios from 'axios'

function App() {
  const [temperature, setTemperature] = useState()

  setInterval(async function() {
    const newTemperature = await axios.get('https://www.voacap.com/geo/weather.html?city=Vaasa')
    setTemperature(newTemperature.data[0].temperature)
  }, 1500)

  return (
    <div className="App">
      <p>learn react</p>
      <Weather temperature={temperature}/>
    </div>
  );
}

export default App;
